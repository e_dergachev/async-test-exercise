const { get } = require('axios');
const headers = { 'Content-Type' : 'application/json'};

describe('Testing if second\'s output on first\'s output is always the first\'s input', () => {
    it ('should return the proper number', async () => {
        for (let i = 0; i < 7; i++) { //checking it 7 times with random numbers
            const firstsInput = Math.floor(Math.random() * 100) + 1;
            const {data: firstsOutput} = await get(`http://kodaktor.ru/api2/there/${firstsInput}`, headers);
            const {data: secondsOutput} = await get(`http://kodaktor.ru/api2/andba/${firstsOutput}`, headers);
            expect(secondsOutput).toBe(firstsInput);
        }
    });
});
